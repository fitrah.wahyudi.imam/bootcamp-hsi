package main

import (
	"fmt"

	"log"

	"net/http"
)

func index(w http.ResponseWriter, r *http.Request) {

	fmt.Fprintf(w, "Hi, this is running for Wahyudi!")

}

func about(w http.ResponseWriter, r *http.Request) {

	fmt.Fprintf(w, "Hi, this is running for Wahyudi!")

}

func main() {

	http.HandleFunc("/", index)

	http.HandleFunc("/about", about)

	log.Println("application started at port :7000")

	log.Fatal(http.ListenAndServe(":7000", nil))

}
