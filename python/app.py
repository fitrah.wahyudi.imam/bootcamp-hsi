from flask import Flask

app = Flask(__name__)
app.config["DEBUG"] = True
port = 80
index = '''
<!DOCTYPE html>
<html>
    <head>
        <title>Test Flask</title>
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <style>
            html, body {
                height: 100%;
            }
            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }
            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }
            .content {
                text-align: center;
                display: inline-block;
            }
            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Test Flask</div>
                <p>Wahyudi</p>
            </div>
        </div>
    </body>
</html>
'''

@app.route('/')
def get_index():
    return index

if __name__ == '__main__':
    # print('Starting Server in Port', port)
    app.run(host='0.0.0.0', port=port)